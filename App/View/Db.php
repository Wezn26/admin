<?php
namespace App\View;

use App\Exceptions\TestException1;
use PDO;
use PDOException;


class Db
{
  use Singleton;
  protected PDO $dbh;

  protected function __construct()
  {
    try {
      $config = require __DIR__ . './../config.php';
      $this->dbh = new PDO($config['dsn'], $config['user'], $config['password']);
    } catch (PDOException $error) {
      throw new TestException1("Error Processing Request", 1);
            
    }    
  }

  public function execute(string $sql, $data)
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    return $res;
  }

  public function query($class, string $sql, array $data = [])
  {
    $sth = $this->dbh->prepare($sql);
    $res = $sth->execute($data);
    if (!$res) {
      throw new DbException($sql, 'Запрос не может быть выполнен!!!');
    } else {
      return $sth->fetchAll(PDO::FETCH_CLASS, $class);
    }
    
  }

  public function getLastId()
  {
    return $this->dbh->lastInsertId();
  }
}
