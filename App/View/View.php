<?php
namespace App\View;

class View
{
  protected $data = [];
  /**
   * @deprecated
   */

  public function assign($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __get($name)
  {
    return $this->data[$name] ?? null;
  }

  public function __isset($name)
  {
    return isset($this->data[$name]);
  }

  public function render($template)
  {
    ob_start();
    include __DIR__ . '/../templates/' . $template;
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }

  public function renderDisplay($template)
  {
    echo $this->render($template);
  }

  public function display($template)
  {
    include __DIR__ . '/../templates/' . $template;
  }
}
