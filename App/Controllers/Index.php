<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Article;
use Runn\ValueObjects\Values\IntValue;

class Index extends Controller
{
  protected function handle()
  {
    // $value = new IntValue(42);
    // echo json_encode($value);
    // die;
    $articles = Article::findAll();
    //$view->assign('articles', $articles);
    $this->view->articles = $articles;

    $this->view->display('index.php');
  }
}
