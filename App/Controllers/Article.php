<?php
namespace App\Controllers;

use App\Controllers\Controller;
use App\Exceptions\TestException1;
use App\Exceptions\TestException2;
use App\View\View;



class Article extends Controller
{
  // protected function access() : bool
  // {
  //   return isset($_GET['name']) && 'Vasya' == $_GET['name'];
  // }
  protected function handle()
  {
    $article = \App\Models\Article::findById($_GET['id']);
    //$view->assign('articles', $articles);
    $this->view->article = $article;

    //throw new TestException2();    

    $this->view->display('article.php');
  }
}
