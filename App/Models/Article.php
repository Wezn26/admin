<?php
namespace App\Models;
use App\View\Db;
use App\Models\Model;

class Article extends Model
{
  public const TABLE = 'news';

  public $title;
  public $content;
}
