<?php
namespace App\Models;
use App\View\Db;
abstract class Model
{
  const TABLE = '';
  public $id;

  public static function findAll()
  {
    $db = Db::instance();
    $class = static::class;
    $sql = 'SELECT * FROM ' . static::TABLE;    
    return $db->query($class, $sql, []);
  }
  public static function findById($id)
  {
    $db = Db::instance();
    $class = static::class;
    $sql = 'SELECT * FROM ' . static::TABLE . ' WHERE id=:id';    
    $value = [':id'=> $id];
    $data = $db->query($class, $sql, $value);
    return $data ? $data[0] : null;
  }

  public function isNew()
  {
    return empty($this->id);
  }

  public function insert()
  {
    if (!$this->isNew()) {
      return;
    }

    $columns = [];
    $values = [];
    foreach ($this as $key => $value) {
      if ('id' == $key) {
        continue;
      }
      $columns[] = $key;
      $values[':' . $key] = $value;
    }
    $sql = 'INSERT INTO ' . static::TABLE . '
    ('. implode(",", $columns) .')
    VALUES
    ('. implode(",", array_keys($values)) .')
    ';

    $db = Db::instance();
    $db->execute($sql, $values);
    $this->id = $db->getLastId();
  }
}
