<?php 
require __DIR__ . '/../App/autoload.php';

function div($x, $y)
{
  try {
    if (0 == $y) {
      throw new DivisionByZeroError('Деление на ноль!!!');
      
    }
    return $x / $y;
  } catch (\Throwable $error) {
    echo $error->getMessage();
    return null;
  } //finally {
    //echo 'Это была функция деления!!!';
  //}
}

var_dump(div(6, 2));