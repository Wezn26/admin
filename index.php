<?php

use App\Exceptions\TestException1;
use App\Exceptions\TestException2;

require __DIR__ . '/App/autoload.php';
$uri = $_SERVER['REQUEST_URI'];
$parts = explode('/', $uri);
//var_dump($parts);die;
$ctrl = $parts[1] ? ucfirst($parts[1]) : 'Index';

// $ex = new \App\View\DbException('Сломалась БД', 100);
// var_dump($ex->getMessage());

//use App\Controllers\Index;
// var_dump($_SERVER['REQUEST_URI']);die;


try {
  $ctrl = $_GET['ctrl'] ?? 'Index';
  $class = '\App\Controllers\\' . $ctrl;
  $ctrl = new $class;
  $ctrl();
} catch (\App\View\DbException $error) {
  echo 'Ошибка в БД при выполнении запроса "'. $error->getQuery() .'": ' . $error->getMessage();
  die;
} catch (\App\Exceptions\TestException1 | TestException2 $error) {
  echo 'Тестовая ошибка!!!' . $error->getMessage();
  die;
} catch (Exception $error) {
  echo 'Неизвестная ошибка!!!' . $error->getMessage();
  die;
}
